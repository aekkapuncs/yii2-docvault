<?php

namespace diggindata\docvault\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UnauthorizedHttpException;
use yii\web\UploadedFile;

use diggindata\docvault\models\Document;
use diggindata\docvault\models\DocumentPermissionsForm;
use diggindata\docvault\models\DocumentSearch;
use diggindata\docvault\models\Log;
use diggindata\docvault\models\Permission;
/**
 * DocumentController implements the CRUD actions for Document model.
 */
class DocumentController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [ // {{{ 
                'class' => AccessControl::className(),
                //'only' => ['index','view','view-file','create','update','checkin','checkout','delete'],
                'rules' => [
                    [
                        'actions' => ['index','view','view-file','create','update','update-permissions','checkin','checkout','delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ], // }}} 
            'verbs' => [ // {{{ 
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ], // }}} 
        ];
    }

    // {{{ actionIndex
    /**
     * Lists all Document models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DocumentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    } // }}} 
    // {{{ actionView
    /**
     * Displays a single Document model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    } // }}} 
    // {{{ actionViewFile
    /**
     * Downloads a single Document model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewFile($id)
    {
        $model = $this->findModel($id);
        if(!$model->mayView)
            throw new UnauthorizedHttpException(Yii::t('docvault','You don\'t have the permission to view the document {realname}.', ['realname'=>$model->realname]));
        $realname = rawurlencode( $model->realname );
        // get the filename
        $filename = Yii::getAlias($this->module->dataDir) . DIRECTORY_SEPARATOR . $model->id . '.dat';
        if(substr($model->mimeType, 0, 6)==='image/') {
            header ("Content-Type: ".$model->mimeType);
             
        } else {
            // send headers to browser to initiate file download
            header ("Content-Type: application/octet-stream"); 
            header ("Content-Disposition: attachment; filename=$realname"); 
        }

        readfile($filename); 
        Yii::$app->end();
    } // }}} 
    // {{{ actionCreate
    /**
     * Creates a new Document model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Document();
        if(!empty($_GET['Document'])) {
            $model->load(Yii::$app->request->get());
            unset($_GET['Document']);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if($model->file) {
                $model->realname = $model->file->name;
                $model->mimeType = $model->file->type;
            }
            $isNewRecord = $model->isNewRecord;
            if ($model->file and $model->save()) {                
                // Save uploaded file
                $model->file->saveAs(Yii::getAlias($this->module->dataDir) . DIRECTORY_SEPARATOR . $model->id . '.dat');
                if ($isNewRecord) {
                    // Create permissions for user on doc.
                    $perm = new Permission;
                    $perm->documentId = $model->id;
                    $perm->userId = Yii::$app->user->id;
                    $perm->rights = Permission::RIGHT_VIEW;
                    $perm->save();
                    $perm = new Permission;
                    $perm->documentId = $model->id;
                    $perm->userId = Yii::$app->user->id;
                    $perm->rights = Permission::RIGHT_MODIFY;
                    $perm->save();
                }
                Yii::$app->session->addFlash('success', [Yii::t('docvault','Create New Document'), Yii::t('docvault', 'The document {realname} has been created.', ['realname'=>$model->realname])]);
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } 
        return $this->render('create', [
            'model' => $model,
        ]);
    } // }}}
    // {{{ actionUpdate
    /**
     * Updates an existing Document model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if($model->ownerId !== Yii::$app->user->id)
            throw new UnauthorizedHttpException(Yii::t('docvault','You are not the owner of the requested document {realname}.', ['realname'=>$model->realname]));
        if(!$model->mayModify)
            throw new UnauthorizedHttpException(Yii::t('docvault','You don\'t have the permission to modify the document {realname}.', ['realname'=>$model->realname]));
        if($model->status>0)
            throw new UnauthorizedHttpException(Yii::t('docvault','The requested document {realname} is currently checked out.', ['realname'=>$model->realname]));
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', [Yii::t('docvault','Update Document'), Yii::t('docvault', 'The document {realname} has been updated.', ['realname'=>$model->realname])]);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    } // }}} 
    // {{{ actionCheckout
    /**
     * Checks out a Document model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionCheckout($id)
    {
        $model = $this->findModel($id);
        if($model->status>0)
            throw new UnauthorizedHttpException('The requested document is currently checked out.');
        if (!$model->mayModify) 
            throw new UnauthorizedHttpException("You don't have 'Modify' permissions for this document.");

        $model->status = Yii::$app->user->id;
        $model->save();
        $realname = rawurlencode( $model->realname );
        // get the filename
        $filename = Yii::getAlias($this->module->dataDir) . DIRECTORY_SEPARATOR . $model->id . '.dat';

        // send headers to browser to initiate file download
        header ("Content-Type: application/octet-stream"); 
        header ("Content-Disposition: attachment; filename=$realname"); 
        readfile($filename); 
        Yii::$app->session->addFlash('success', [Yii::t('docvault','Checkout Document'), Yii::t('docvault', 'The document {realname} has been checked out.', ['realname'=>$model->realname])]);
        Yii::$app->end();
            
    } // }}} 
    // {{{ actionCheckin
    /**
     * Checks in a Document model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionCheckin($id)
    {
        $model = $this->findModel($id);
        if($model->status!==Yii::$app->user->id)
            throw new UnauthorizedHttpException('The requested document is currently checked out by another user.');
        $log = new Log();
        $log->documentId = $id;

        if ($log->load(Yii::$app->request->post())) {
            $log->file = UploadedFile::getInstance($log, 'file');
            if($log->file)
                $model->realname = $log->file->name;
            if ($log->file && $log->save()) {  
                $log->file->saveAs(Yii::getAlias($this->module->dataDir) . DIRECTORY_SEPARATOR . $model->id . '.dat');
                $model->status = 0;
                $model->save(false);
                Yii::$app->session->addFlash('success', [Yii::t('docvault','Checkin Document'), Yii::t('docvault', 'The document {realname} has been checked in.', ['realname'=>$model->realname])]);
                return $this->redirect(['view', 'id'=>$model->id]);
            }    
        } else {
            return $this->render('checkin', [
                'document' => $model,
                'model' => $log,
            ]);
        }
    } // }}}
    // {{{ actionUpdatePermissions
    public function actionUpdatePermissions($id)
    {
        $document = $this->findModel($id);
        // Is user the owner?
        if($document->ownerId !== Yii::$app->user->id)
            throw new UnauthorizedHttpException(Yii::t('docvault','You are not the owner of the requested document.'));
        // Is document not checked out?
        if($document->status>0)
            throw new UnauthorizedHttpException(Yii::t('docvault','The requested document is currently checked out.'));

        $permissions = Permission::find()->indexBy('id')->where(['documentId'=>$id])->all();

        $model = new DocumentPermissionsForm;

        $model->documentId = $id;
        foreach($permissions as $permission) {
            if($permission->rights==Permission::RIGHT_VIEW)
                $model->viewUsers[] = $permission->userId;
            elseif($permission->rights==Permission::RIGHT_MODIFY)
                $model->modifyUsers[] = $permission->userId;
        }
        
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $transaction = $this->module->db->beginTransaction();
            try {
                $permissions = $document->permissions;
                foreach($permissions as $permission)
                    $permission->delete();
                foreach($model->viewUsers as $userId) {
                    $permission = new Permission;
                    $permission->documentId = $document->id;
                    $permission->userId = $userId;
                    $permission->rights = Permission::RIGHT_VIEW;
                    if(!$permission->save())
                        throw new \Exception('Error saving permission');
                }
                foreach($model->modifyUsers as $userId) {
                    $permission = new Permission;
                    $permission->documentId = $document->id;
                    $permission->userId = $userId;
                    $permission->rights = Permission::RIGHT_MODIFY;
                    if(!$permission->save())
                        throw new \Exception('Error saving permission');
                }
                $transaction->commit();
                Yii::$app->session->addFlash('success', [Yii::t('docvault','Update Document Permissions'), Yii::t('docvault','The permissions have been updated.')]);
                return $this->redirect(['view', 'id'=>$document->id]);
            } catch (\Exception $e) {
                $transaction->rollBack();
                return $this->render('updatePermissions', [
                    'document'=>$document, 
                    'model'=>$model, 
                ]);        
            }
        } else {
            return $this->render('updatePermissions', [
                'document'=>$document, 
                'model'=>$model, 
            ]);        
        }
    } // }}}
    // {{{ actionDelete
    /**
     * Deletes an existing Document model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if($model->ownerId !== Yii::$app->user->id or !$model->mayModify)
            throw new UnauthorizedHttpException(Yii::t('docvault','You don\'t have the permission to delete the document {realname}.', ['realname'=>$model->realname]));
        if($model->delete())
            Yii::$app->session->addFlash('success', [Yii::t('docvault','Delete Document'), Yii::t('docvault','The document {realname} has been deleted.', ['realname'=>$model->realname])]);
        return $this->redirect(['index']);
    } // }}} 
    // {{{ findModel
    /**
     * Finds the Document model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Document the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Document::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested document does not exist.');
        }
    } // }}} 
}
