<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DocumentCategory */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'DocVault', 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('docvault', 'Document Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-category-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="btn-group" role="group" aria-label="..." style="padding: 10px">
        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('docvault', 'New Document'), ['document/create', 'Document[categoryId]' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> ' . Yii::t('docvault', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-remove"></span> ' . Yii::t('docvault', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'label' => Yii::t('docvault','Documents'),
                'format' => 'html',
                'value' => Html::a(count($model->documents), ['document/index', 'DocumentSearch[categoryId]'=>$model->id]),
            ],
        ],
    ]) ?>

</div>
