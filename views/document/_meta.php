<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
?>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'categoryId',
                'format' => 'html',
                'value' => Html::a($model->category->name, ['document-category/view', 'id'=>$model->categoryId]),
            ],
            [
                'attribute' => 'ownerId',
                'value' => $model->ownedByUser->username,
            ],
            'realname',
            'mimeType',
            [
                'attribute' => 'created',
                'value' => Yii::$app->formatter->asDatetime($model->created, 'medium'),
            ],
            'description',
            'comment:ntext',
            [
                'attribute' => 'status',
                'value' => !is_null($model->checkedOutByUser) ? $model->checkedOutByUser->username : Yii::t('docvault','(not yet)'),
            ],
        ],
    ]) ?>


