<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Document */

$this->title = Yii::t('docvault', 'Update Document:') . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'DocVault', 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('docvault', 'Documents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('docvault', 'Update');
?>
<div class="document-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
