<?php
// \yii\helpers\BaseVarDumper::dump($permissions, 10, true);

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Permission;
use dektrium\user\models\User;

$this->title = Yii::t('docvault', 'Update Document Permissions:') . ' ' . $document->id;
$this->params['breadcrumbs'][] = ['label' => 'DocVault', 'url' => ['default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('docvault', 'Documents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $document->id, 'url' => ['view', 'id' => $document->id]];
$this->params['breadcrumbs'][] = Yii::t('docvault', 'Update Permissions');

?>

<h1><?= Html::encode($this->title) ?></h1>

<?php $form = ActiveForm::begin([
    'id' => 'document-permissions-form',
    'options' => ['class' => 'form-horizontal'],
]);
?>
<?php $userOptions = ArrayHelper::map(User::find()->all(), 'id', 'username');
$userOptions = ArrayHelper::merge([0=>Yii::t('docvault','All Users')], $userOptions);
// DEBUG yii\helpers\VarDumper::dump($userOptions, 10, true);
?>

<?= $form->field($model, 'documentId')->textInput(['disabled'=>'disabled']) ?>

<?= $form->field($model, 'viewUsers')->dropDownList($userOptions, ['multiple'=>'multiple']); ?>

<?= $form->field($model, 'modifyUsers')->dropDownList($userOptions, ['multiple'=>'multiple']); ?>

<div class="form-group">
    <?= Html::submitButton(Yii::t('docvault', 'Update'), ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>

