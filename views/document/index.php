<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use app\widgets\TileView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('docvault', 'Documents');
$this->params['breadcrumbs'][] = ['label' => 'DocVault', 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;

$createLink = ['create'];
if (!empty($_GET['DocumentSearch']['categoryId']))
    $createLink['Document[categoryId]'] = $_GET['DocumentSearch']['categoryId'];
?>

<style type="text/css">
<!--
/* {{{ */
.tile {
  width: 100%;
  display: inline-block;
  box-sizing: border-box;
  background: #fff;
  padding: 20px;
  margin-bottom: 30px;
}
.tile .title {
  margin-top: 0px;
}
.tile.purple, .tile.blue, .tile.red, .tile.orange, .tile.green {
  color: #fff;
}
.tile.purple {
  background: #5133AB;
}
.tile.purple:hover {
  background: #3e2784;
}
.tile.red {
  background: #AC193D;
}
.tile.red:hover {
  background: #7f132d;
}
.tile.green {
  background: #00A600;
}
.tile.green:hover {
  background: #007300;
}
.tile.blue {
  background: #2672EC;
}
.tile.blue:hover {
  background: #125acd;
}
.tile.orange {
  background: #DC572E;
}
.tile.orange:hover {
  background: #b8431f;
}
.tile h3, .tile a {
  color: white;
}
.tile a:hover {
  color: #eee;
}
/* }}} */
// -->
</style>
<div class="document-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('docvault', 'Create New Document', ['modelClass' => 'Document']), $createLink, ['class' => 'btn btn-success']) ?>
        <?= Html::a('<span class="glyphicon glyphicon-search"></span> ' . Yii::t('docvault', 'Advanced Search'), '#', ['class' => 'btn btn-primary search-btn']) ?>
    </p>

    <?= TileView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item col-sm-4'],
        'itemView' => function ($model, $key, $index, $widget) {
            return $this->render('_view', ['model'=>$model, 'key'=>$key, 'index'=>$index, ]);
        },
    ]) ?>

</div>

<?php
$script = <<< JS
$('.search-btn').on('click', function(e) {
    $('.document-search-form').toggle(400);
    return false;
});
JS;
$this->registerJs($script);
// where $position can be View::POS_READY (the default), 
// or View::POS_HEAD, View::POS_BEGIN, View::POS_END
// ?>
