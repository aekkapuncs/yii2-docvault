<?php

use yii\db\Schema;
use diggindata\docvault\migrations\Migration;

class m150202_124833_init extends Migration
{
    public function up()
    {
        // {{{ category
        $this->createTable('{{%dv_category}}', [
            'id'            => Schema::TYPE_PK,
            'name'          => Schema::TYPE_STRING . '(255) NOT NULL', 
        ], $this->tableOptions);
        // }}}
        // {{{document
        $this->createTable('{{%dv_document}}', [
            'id'            => Schema::TYPE_PK,
            'categoryId'    => Schema::TYPE_INTEGER . ' DEFAULT 0',
            'ownerId'       => Schema::TYPE_INTEGER . ' DEFAULT 0',
            'realname'      => Schema::TYPE_STRING . '(255) NOT NULL', 
            'mimeType'      => Schema::TYPE_STRING . '(255) NOT NULL', 
            'created'       => Schema::TYPE_DATETIME . " NOT NULL DEFAULT '0000-00-00 00:00:00'", 
            'description'   => Schema::TYPE_STRING . '(255) NULL', 
            'comment'       => Schema::TYPE_TEXT . ' NULL', 
            'status'        => Schema::TYPE_INTEGER . ' DEFAULT 0',
        ], $this->tableOptions);
        $this->addForeignKey('fk_doccat_doc', '{{%dv_document}}', 'categoryId', '{{%dv_category}}', 'id', 'CASCADE', 'CASCADE');
        // }}} 
        // {{{log
        $this->createTable('{{%dv_log}}', [
            'id'            => Schema::TYPE_PK,
            'documentId'    => Schema::TYPE_INTEGER . ' NOT NULL',
            'modifiedOn'    => Schema::TYPE_DATETIME . " NOT NULL DEFAULT '0000-00-00 00:00:00'", 
            'modifiedBy'    => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'note'       => Schema::TYPE_TEXT . ' NULL', 
        ], $this->tableOptions);
        $this->addForeignKey('fk_doc_log', '{{%dv_log}}', 'documentId', '{{%dv_document}}', 'id', 'CASCADE', 'CASCADE');
        // }}}
        // {{{ permission
        $this->createTable('{{%dv_permission}}', [
            'id'            => Schema::TYPE_PK,
            'documentId'    => Schema::TYPE_INTEGER . ' NOT NULL',
            'userId'        => Schema::TYPE_INTEGER . " NOT NULL DEFAULT 0", 
            'rights'    => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
        ], $this->tableOptions);
        $this->addForeignKey('fk_doc_perm', '{{%dv_permission}}', 'documentId', '{{%dv_document}}', 'id', 'CASCADE', 'CASCADE');
        // }}}
    }

    public function down()
    {
        $this->dropTable('{{%dv_permission}}');
        $this->dropTable('{{%dv_log}}');
        $this->dropTable('{{%dv_document}}');
        $this->dropTable('{{%dv_category}}');
    }
}
