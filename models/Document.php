<?php

namespace diggindata\docvault\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%document}}".
 *
 * @property integer $id
 * @property integer $categoryId
 * @property integer $ownerId
 * @property string $realname
 * @property string $mimeType
 * @property string $created
 * @property string $description
 * @property string $comment
 * @property integer $status
 *
 * @property Category $category
 */
class Document extends \yii\db\ActiveRecord
{
    /**
     * @var UploadedFile file attribute
     */
    public $file;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%dv_document}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['categoryId','description'], 'required'],
            [['ownerId', 'status'], 'integer'],
            [['categoryId','ownerId','realname'], 'required', 'on'=>['insert']],
            [['created'], 'safe'],
            [['comment'], 'string'],
            [['realname', 'mimeType', 'description'], 'string', 'max' => 255],
            [['file'], 'file', 'skipOnEmpty'=>false, 'on'=>'insert'],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord) {
                $this->ownerId = Yii::$app->user->id;
                $this->created = date('Y-m-d H:i:s');
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('docvault', 'ID'),
            'categoryId' => Yii::t('docvault', 'Category'),
            'ownerId' => Yii::t('docvault', 'Owner'),
            'realname' => Yii::t('docvault', 'Original File Name'),
            'mimeType' => Yii::t('docvault', 'Type'),
            'created' => Yii::t('docvault', 'Date First Checked-In'),
            'description' => Yii::t('docvault', 'Description of Contents'),
            'comment' => Yii::t('docvault', "Author's Note"),
            'status' => Yii::t('docvault', 'Checked out by'),
            'file' => Yii::t('docvault', 'Dokument'),
        ];
    }

    public function afterDelete()
    {
        parent::afterDelete();
        // delete file
        unlink(Yii::getAlias(Yii::$app->getModule('docvault')->dataDir) . DIRECTORY_SEPARATOR . $this->id . '.dat');

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(DocumentCategory::className(), ['id' => 'categoryId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwnedByUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'ownerId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckedOutByUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'status']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogs()
    {
        return $this->hasMany(Log::className(), ['documentId' => 'id'])->orderBy('modifiedOn DESC');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPermissions()
    {
        return $this->hasMany(Permission::className(), ['documentId' => 'id']);
    }

    public function getMayModify()
    {
        foreach($this->permissions as $permission) {
            if($permission->userId==Yii::$app->user->id and $permission->rights == Permission::RIGHT_MODIFY)
                return true;
            if($permission->userId==0 and $permission->rights == Permission::RIGHT_MODIFY)
                return true;
        }
        return false;
    }

    public function getMayView()
    {
        foreach($this->permissions as $permission) {
            if($permission->userId==Yii::$app->user->id and $permission->rights == Permission::RIGHT_VIEW)
                return true;
            if($permission->userId==0 and $permission->rights == Permission::RIGHT_VIEW)
                return true;
        }
        return false;
    }

}
