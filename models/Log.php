<?php

namespace diggindata\docvault\models;

use Yii;

/**
 * This is the model class for table "{{%log}}".
 *
 * @property integer $id
 * @property integer $documentId
 * @property string $modifiedOn
 * @property integer $modifiedBy
 * @property string $note
 *
 * @property Document $document
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * @var UploadedFile file attribute
     */
    public $file;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%dv_log}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['documentId'], 'required'],
            [['documentId', 'modifiedBy'], 'integer'],
            [['modifiedOn'], 'safe'],
            [['note'], 'string'],
            [['file'], 'file', 'skipOnEmpty'=>false],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord) {
                $this->modifiedBy = Yii::$app->user->id;
                $this->modifiedOn = date('Y-m-d H:i:s');
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('docvault', 'ID'),
            'documentId' => Yii::t('docvault', 'Document ID'),
            'modifiedOn' => Yii::t('docvault', 'Date of revision'),
            'modifiedBy' => Yii::t('docvault', 'ID of user modifying file'),
            'note' => Yii::t('docvault', 'Description of modification'),
            'file' => Yii::t('docvault', 'File'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->hasOne(Document::className(), ['id' => 'documentId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\dektrium\user\models\User::className(), ['id' => 'modifiedBy']);
    }
}
