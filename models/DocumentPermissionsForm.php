<?php
namespace diggindata\docvault\models;

use Yii;

class DocumentPermissionsForm extends \yii\base\Model
{

    public $documentId;
    public $viewUsers=[];
    public $modifyUsers=[];

    public function rules()
    {
        return [
            [['documentId'], 'required'],
            [['documentId'], 'integer'],
            [['viewUsers','modifyUsers'], 'safe'],
        ];

    }

    public function attributeLabels()
    {
        return [
            'documentId' => Yii::t('docvault', 'Document'),
            'viewUsers' => Yii::t('docvault', 'Users with View Rights'),
            'modifyUsers' => Yii::t('docvault', 'Users with Modify Rights'),
        ];
    }
}
